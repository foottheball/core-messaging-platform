"""Created by sgoswami on 5/17/17 as part of cmp"""
import json

from models.user.user import User


class UserResource:
    def on_get(self, req, res):
        user = User('soumasish@gmail.com', 'Soumasish', 'Goswami')
        res.body = json.dumps(user.serialize())

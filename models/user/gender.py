"""Created by sgoswami on 5/17/17 as part of cmp"""
from enum import Enum

class Gender(Enum):
    FEMALE = 0
    MALE = 1


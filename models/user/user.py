"""Created by sgoswami on 5/17/17 as part of cmp"""
from peewee import *
from models.base.base_model import BaseModel


class User(BaseModel):

    _email = CharField(max_length=255, unique=True)
    _first_name = CharField(max_length=255)
    _last_name = CharField(max_length=255)
    _profile_image = CharField(max_length=500, default=None)
    _gender = CharField(max_length=255, default=None)

    def __init__(self, email, first_name, last_name):
        self.email = email
        self.first_name = first_name
        self.last_name = last_name
        self.profile_image = None
        self.gender = None

    def set_profile_image(self, profile_image_url):
        self.profile_image = profile_image_url

    def set_gender(self, gender):
        self.gender = gender

    def serialize(self):
        user_map = {}
        for attr, value in self.__dict__.items():
            user_map[attr] = value
        return user_map


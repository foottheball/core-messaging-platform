"""Created by sgoswami on 5/17/17 as part of cmp"""
from peewee import *


db = SqliteDatabase('cmp_v0.db')


def before_request_handler():
    db.connect()


def after_request_handler():
    db.close()

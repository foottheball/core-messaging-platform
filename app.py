import falcon
from resources.user.user_resource import UserResource

api = falcon.API()
api.add_route('/user', UserResource())

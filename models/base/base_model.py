"""Created by sgoswami on 5/17/17 as part of cmp"""
from dao.config import *
from datetime import datetime


class BaseModel(Model):
    _create_at = DateTimeField(default=datetime.now())
    _modified_at = DateTimeField(default=datetime.now())

    class Meta:
        database = db
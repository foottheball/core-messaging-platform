"""Created by sgoswami on 5/17/17 as part of cmp"""


class Room:
    def __init__(self, name, team, location):
        self._name = name
        self._team = team
        self._location = location
        self._users = None
